package com.aditya.kafka.web.config;

import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.aditya.kafka.events")
@PropertySources(
        @PropertySource("classpath:web.application.properties")
)
public class WebConfiguration {

}
