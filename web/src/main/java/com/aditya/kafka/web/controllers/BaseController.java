package com.aditya.kafka.web.controllers;

import com.aditya.kafka.events.constants.TopicNames;
import com.aditya.kafka.events.models.entity.Employee;
import com.aditya.kafka.events.services.EmployeeService;
import com.aditya.kafka.events.services.FileService;
import com.aditya.kafka.events.services.KafkaProducerService;
import com.aditya.kafka.events.services.RecordService;
import com.aditya.kafka.web.models.File;
import com.aditya.kafka.web.models.FileRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Controller
public class BaseController {

    @Autowired
    KafkaProducerService kafkaProducerService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    FileService fileService;

    @Autowired
    RecordService recordService;

    /*@PostMapping("/save")
    public String saveEmployee(@RequestBody Employee employee){
        employeeService.saveEmployee(employee);
        return "Employee details saved";
    }

    @GetMapping("/employees")
    public List<Employee> getEmployees(){
        return employeeService.getEmployees();
    }*/

    @GetMapping("/")
    public String index(){
        return "readfile";
    }

    @PostMapping("uploadfile")
    public String readFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes){
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        System.out.println(fileName);
        if(file.isEmpty()){
            System.out.println("No file found");
            redirectAttributes.addFlashAttribute("error", "Please add a file");
            return "redirect:/";
        }
        if(!fileName.endsWith(".csv")){
            redirectAttributes.addFlashAttribute("error", "Only csv file accepted");
            return "redirect:/";
        }

        try {
            InputStream is = file.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String str;
            int count = 0;
            List<FileRecord> records = new ArrayList<>();
            while ((str=reader.readLine()) != null){
                count++;
                if(str.split(",").length!=5){
                    records.add(FileRecord.builder().errorData(str).build());
                }
                else{
                    String[] data = str.split(",");
                    records.add(FileRecord.builder()
                            .name(data[0])
                            .designation(data[1])
                            .currencyCode(data[2])
                            .salary(Double.parseDouble(data[3]))
                            .yearlyOrMonthly(data[4])
                            .build());
                }
            }
            File uploadedFile = File.builder().name(fileName).length(count).records(records).build();
            System.out.println("File object created, trying to send to kafka.");
            kafkaProducerService.send(TopicNames.FILE_READER, uploadedFile.getName(), uploadedFile);

        } catch (IOException e) {
            redirectAttributes.addFlashAttribute("error1", "Error occured while reading file");
        }
        redirectAttributes.addFlashAttribute("message", "File uploaded successfully");
        return "redirect:/";
    }

    @GetMapping("/getFiles")
    public String getFiles(Model model){
        model.addAttribute("files", fileService.getFiles());
        return "fileslist";
    }

    @GetMapping("/getRecords/{id}")
    public String getRecordsForFile(Model model, @PathVariable("id") Long fileId){

        model.addAttribute("filename",fileService.getFileById(fileId).getFileName());

        model.addAttribute("records", recordService.getRecordsByFileId(fileId));
        return "recordslist";
    }

    @GetMapping("/get")
    public String getEmployees(Model model){
        model.addAttribute("employees", employeeService.getEmployees());
        return "employeelist";
    }
}
