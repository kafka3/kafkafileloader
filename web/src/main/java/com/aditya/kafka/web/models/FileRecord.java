package com.aditya.kafka.web.models;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class FileRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String designation;
    private String currencyCode;
    private Double salary;
    private String yearlyOrMonthly;
    private String errorData;
}
