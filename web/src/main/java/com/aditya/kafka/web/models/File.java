package com.aditya.kafka.web.models;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class File implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private Integer length;
    private List<FileRecord> records;
}
