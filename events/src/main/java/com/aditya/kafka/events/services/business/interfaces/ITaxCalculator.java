package com.aditya.kafka.events.services.business.interfaces;

public interface ITaxCalculator {

    Double processSalary(Double salary, String monthlyOrYearly);
    String getCurrencyCode();
}
