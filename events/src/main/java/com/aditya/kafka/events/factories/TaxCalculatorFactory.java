package com.aditya.kafka.events.factories;

import com.aditya.kafka.events.services.business.interfaces.ITaxCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TaxCalculatorFactory {

    @Autowired
    private List<ITaxCalculator> taxCalculators;

    private final Map<String, ITaxCalculator> taxCalculatorMap = new HashMap<>();

    @PostConstruct
    private void init(){
        taxCalculators.forEach(t->taxCalculatorMap.put(t.getCurrencyCode(), t));
    }

    public ITaxCalculator getTaxCalculator(String currencyCode){
        ITaxCalculator taxCalculator = taxCalculatorMap.get(currencyCode);
        if(taxCalculator != null){
            return taxCalculator;
        }
        return null;
    }
}
