package com.aditya.kafka.events.services.business;

import com.aditya.kafka.events.constants.CommonConstants;
import com.aditya.kafka.events.services.business.interfaces.ITaxCalculator;
import org.springframework.stereotype.Service;

@Service
public class INRTaxCalculator implements ITaxCalculator {
    @Override
    public Double processSalary(Double salary, String monthlyOrYearly) {

        Double tax = 0d;

        if(monthlyOrYearly.equalsIgnoreCase(CommonConstants.MONTHLY)){
            salary = salary * 12;
        }

        if(salary <= 250000){
            tax = 0d;
        }
        else if(salary <= 500000){
            tax = 0 + ((salary - 250000)*0.05);
        }
        else if(salary <= 1000000){
            tax = 12500 + ((salary - 500000)*0.2);
        }
        else if(salary > 1000000){
            tax = 112500 + ((salary - 1000000)*0.3);
        }

        if(monthlyOrYearly.equalsIgnoreCase(CommonConstants.MONTHLY)){
            tax = tax/12;
        }

        return tax;
    }

    @Override
    public String getCurrencyCode() {
        return CommonConstants.INR;
    }
}
