package com.aditya.kafka.events.models.entity;

import com.aditya.kafka.events.models.entity.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "record_status")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecordStatus extends BaseModel {

    @Column(name = "status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
