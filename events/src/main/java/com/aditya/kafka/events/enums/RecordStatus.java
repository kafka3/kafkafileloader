package com.aditya.kafka.events.enums;

public enum RecordStatus {
    LOADED,
    PROCESSING,
    ERROR,
    COMPLETED
}
