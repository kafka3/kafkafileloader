package com.aditya.kafka.events.services;

import com.aditya.kafka.events.models.entity.QRecord;
import com.aditya.kafka.events.models.entity.Record;
import com.aditya.kafka.events.repositories.RecordRepository;
import com.querydsl.core.BooleanBuilder;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordService extends BaseService {

    @Autowired
    RecordRepository repo;

    public void save(Record model){
        if(model!=null){
            prepModel(model);
            repo.save(model);
        }
    }

    public List<Record> getRecords(){
        return repo.findAll();
    }

    public List<Record> getRecordsByFileId(Long fileId){

        QRecord q =  QRecord.record;

        BooleanBuilder where = new BooleanBuilder();
        where.and(q.fileDetails.id.eq(fileId));
        return IteratorUtils.toList(repo.findAll(where).iterator());
    }

    public List<Record> getRecordsbyStatus(Long statusId){
        QRecord q = QRecord.record;

        BooleanBuilder where = new BooleanBuilder();
        where.and(q.recordStatus.id.eq(statusId));
        return IteratorUtils.toList(repo.findAll(where).iterator());
    }
    public List<Record> getRecordsbyFileIdandStatus(Long fileId, Long statusId){
        QRecord q = QRecord.record;

        BooleanBuilder where = new BooleanBuilder();
        where.and(q.fileDetails.id.eq(fileId)).and(q.recordStatus.id.eq(statusId));
        return IteratorUtils.toList(repo.findAll(where).iterator());
    }
}
