package com.aditya.kafka.events.models.entity;

import javax.persistence.*;

@Entity
@Table(name = "file")
public class FileDetails extends BaseModel {

    @Column(name = "file_name")
    private String fileName;

    @ManyToOne
    @JoinColumn(name = "file_status_id")
    private FileStatus status;

    @Column(name = "length")
    private Integer length;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public FileStatus getStatus() {
        return status;
    }

    public void setStatus(FileStatus status) {
        this.status = status;
    }
}
