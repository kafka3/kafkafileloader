package com.aditya.kafka.events.repositories;

import com.aditya.kafka.events.models.entity.FileStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileStatusRepository extends JpaRepository<FileStatus, Long> {
}
