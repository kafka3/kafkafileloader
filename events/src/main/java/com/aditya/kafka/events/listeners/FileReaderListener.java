package com.aditya.kafka.events.listeners;

import com.aditya.kafka.events.constants.TopicNames;
import com.aditya.kafka.events.enums.FileStatus;
import com.aditya.kafka.events.enums.RecordStatus;
import com.aditya.kafka.events.models.entity.FileDetails;
import com.aditya.kafka.events.models.entity.Record;
import com.aditya.kafka.events.models.web.File;
import com.aditya.kafka.events.providers.FileStatusProvider;
import com.aditya.kafka.events.providers.RecordStatusProvider;
import com.aditya.kafka.events.services.FileService;
import com.aditya.kafka.events.services.KafkaProducerService;
import com.aditya.kafka.events.services.RecordService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class FileReaderListener {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private KafkaProducerService producerService;

    @Autowired
    private FileStatusProvider fileStatusProvider;

    @Autowired
    private RecordStatusProvider recordStatusProvider;

    @Autowired
    private FileService fileService;

    @Autowired
    private RecordService recordService;

    @KafkaListener(groupId = "${kafka.groupId}", topics = TopicNames.FILE_READER)
    public void listenMessage(String message){
        try {
            File inputFile = objectMapper.readValue(message, File.class);
            log.info("File received with name: "+inputFile.getName()+" having records: "+inputFile.getLength());

            FileDetails fd = new FileDetails();
            fd.setFileName(inputFile.getName());
            fd.setLength(inputFile.getLength());
            fd.setStatus(fileStatusProvider.getFileStatus(FileStatus.LOADING.toString()));
            fileService.saveFile(fd);

            inputFile.getRecords().forEach(r->{
                Record record = new Record();
                if(r.getErrorData()!=null){
                    record.setErrorDetails("Data with incorrect number of fields"+r.getErrorData());
                }
                else{
                    record.setName(r.getName());
                    record.setDesignation(r.getDesignation());
                    record.setCurrencyCode(r.getCurrencyCode().toUpperCase());
                    record.setSalary(r.getSalary());
                    record.setYearlyOrMonthly(r.getYearlyOrMonthly().toUpperCase());
                    record.setFileDetails(fd);
                    record.setRecordStatus(recordStatusProvider.getRecordStatus(RecordStatus.LOADED.toString()));
                    recordService.save(record);

                    producerService.send(TopicNames.RECORD_PROCESSING_START.toString(), record.toString(), record);
                }

            });

            fd.setStatus(fileStatusProvider.getFileStatus(FileStatus.LOADED.toString()));
            fileService.saveFile(fd);

        } catch (IOException e) {
            log.error("Unable to parse data due to: "+e.getLocalizedMessage());
        }
    }
}
