package com.aditya.kafka.events.services;

import com.aditya.kafka.events.models.entity.BaseModel;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BaseService {

    protected void prepModel(BaseModel model){
        Date date = new Date();
        if(model.getId() == null){
            model.setCreatedDate(date);
        }
        model.setLastUpdatedDate(date);
    }
}
