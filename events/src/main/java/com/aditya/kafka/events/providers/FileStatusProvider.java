package com.aditya.kafka.events.providers;

import com.aditya.kafka.events.models.entity.FileStatus;
import com.aditya.kafka.events.repositories.FileStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class FileStatusProvider {

    @Autowired
    FileStatusRepository repository;

    private final Map<String, FileStatus> fileStatusMap = new HashMap<>();

    @PostConstruct
    private void init(){
        List<FileStatus> fileStatuses = new ArrayList<>();
        fileStatuses.add(FileStatus.builder().status("LOADING").build());
        fileStatuses.add(FileStatus.builder().status("LOADED").build());
        fileStatuses.add(FileStatus.builder().status("PROCESSING").build());
        fileStatuses.add(FileStatus.builder().status("COMPLETED").build());

        repository.saveAll(fileStatuses);

        fileStatuses.forEach(f->fileStatusMap.put(f.getStatus(), f));

    }

    public FileStatus getFileStatus(String status){
        return Optional.of(fileStatusMap.get(status)).orElse(null);
    }
}
