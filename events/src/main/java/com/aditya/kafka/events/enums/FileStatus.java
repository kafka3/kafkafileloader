package com.aditya.kafka.events.enums;

public enum FileStatus {
    LOADING,
    LOADED,
    PROCESSING,
    COMPLETED
}
