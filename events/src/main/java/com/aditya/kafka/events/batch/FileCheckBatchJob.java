package com.aditya.kafka.events.batch;

import com.aditya.kafka.events.models.entity.FileDetails;
import com.aditya.kafka.events.models.entity.FileStatus;
import com.aditya.kafka.events.models.entity.RecordStatus;
import com.aditya.kafka.events.providers.FileStatusProvider;
import com.aditya.kafka.events.providers.RecordStatusProvider;
import com.aditya.kafka.events.services.FileService;
import com.aditya.kafka.events.services.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileCheckBatchJob {

    @Autowired
    private FileService fileService;

    @Autowired
    private FileStatusProvider fileStatusProvider;

    @Autowired
    private RecordService recordService;

    @Autowired
    private RecordStatusProvider recordStatusProvider;

    @Scheduled(fixedDelay = 10000)
    private void checkAndUpdateFileStatus(){

        FileStatus fileStatus = fileStatusProvider.getFileStatus(com.aditya.kafka.events.enums.FileStatus.PROCESSING.toString());
        RecordStatus recordStatus = recordStatusProvider.getRecordStatus(com.aditya.kafka.events.enums.RecordStatus.PROCESSING.toString());

        List<FileDetails> fileDetails = fileService.getFilesByStatus(fileStatus.getId());

        fileDetails.forEach(fd->{
            if(recordService.getRecordsbyFileIdandStatus(fd.getId(), recordStatus.getId()).size() == 0){
                fd.setStatus(fileStatusProvider.getFileStatus(com.aditya.kafka.events.enums.FileStatus.COMPLETED.toString()));
                fileService.saveFile(fd);
            }
        });

    }
}
