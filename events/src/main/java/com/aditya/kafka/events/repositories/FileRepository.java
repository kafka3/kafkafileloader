package com.aditya.kafka.events.repositories;

import com.aditya.kafka.events.models.entity.FileDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends JpaRepository<FileDetails, Long>, QuerydslPredicateExecutor<FileDetails> {
}
