package com.aditya.kafka.events.constants;

public class CommonConstants {

    public static final String YEARLY = "Y";
    public static final String MONTHLY = "M";

    // Currency codes
    public static final String INR = "INR";
    public static final String MYR = "MYR";
}
