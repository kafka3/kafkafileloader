package com.aditya.kafka.events.listeners;

import com.aditya.kafka.events.constants.TopicNames;
import com.aditya.kafka.events.enums.FileStatus;
import com.aditya.kafka.events.enums.RecordStatus;
import com.aditya.kafka.events.factories.TaxCalculatorFactory;
import com.aditya.kafka.events.models.entity.FileDetails;
import com.aditya.kafka.events.models.entity.Record;
import com.aditya.kafka.events.providers.FileStatusProvider;
import com.aditya.kafka.events.providers.RecordStatusProvider;
import com.aditya.kafka.events.services.FileService;
import com.aditya.kafka.events.services.RecordService;
import com.aditya.kafka.events.services.business.interfaces.ITaxCalculator;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class RecordProcessingListener {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TaxCalculatorFactory taxCalculatorFactory;

    @Autowired
    private RecordStatusProvider recordStatusProvider;

    @Autowired
    private FileStatusProvider fileStatusProvider;

    @Autowired
    private RecordService recordService;

    @Autowired
    private FileService fileService;

    @KafkaListener(groupId = "${kafka.groupId}", topics = TopicNames.RECORD_PROCESSING_START)
    public void listenMessage(String message){
        try {
            Record record = objectMapper.readValue(message, Record.class);
            Boolean continueToProcess = true;

            if(!record.getFileDetails().getStatus().equals(FileStatus.PROCESSING)){
                FileDetails fileDetails = record.getFileDetails();
                fileDetails.setStatus(fileStatusProvider.getFileStatus(FileStatus.PROCESSING.toString()));
                fileService.saveFile(fileDetails);
            }

            record.setRecordStatus(recordStatusProvider.getRecordStatus(RecordStatus.PROCESSING.toString()));
            recordService.save(record);

            if(!validateRecord(record)){
                continueToProcess = false;
                record.setRecordStatus(recordStatusProvider.getRecordStatus(RecordStatus.ERROR.toString()));
                recordService.save(record);
            }

            if(continueToProcess){
                ITaxCalculator taxCalculator = taxCalculatorFactory.getTaxCalculator(record.getCurrencyCode());
                record.setTakeHomeSalary(Math.round((record.getSalary() - taxCalculator.processSalary(record.getSalary(), record.getYearlyOrMonthly()))*100.0)/100.0);
                record.setRecordStatus(recordStatusProvider.getRecordStatus(RecordStatus.COMPLETED.toString()));
                recordService.save(record);
            }

        } catch (IOException e) {
            log.error("Unable to parse record due to: "+e.getLocalizedMessage());
        }
    }

    private boolean validateRecord(Record record){

        boolean validRecord = true;

        if(!validCurrencyCodes(record.getCurrencyCode())){
            validRecord = false;
            record.setErrorDetails("Currency code not accepted");
        }

        if(!(record.getYearlyOrMonthly().equalsIgnoreCase("Y")
                ||record.getYearlyOrMonthly().equalsIgnoreCase("M"))){
            validRecord = false;
            record.setErrorDetails("Share either Yearly or Monthly salary");
        }

        return validRecord;

    }

    private boolean validCurrencyCodes(String code){
        return code.equalsIgnoreCase("MYR")||
                code.equalsIgnoreCase("INR");
    }

}
