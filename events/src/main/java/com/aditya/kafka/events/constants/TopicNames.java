package com.aditya.kafka.events.constants;

public class TopicNames {

    public static final String FILE_READER = "file_reader_topic";
    public static final String RECORD_PROCESSING_START = "record_processing_start_topic";
}
