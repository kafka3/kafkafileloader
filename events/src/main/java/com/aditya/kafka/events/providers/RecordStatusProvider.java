package com.aditya.kafka.events.providers;

import com.aditya.kafka.events.models.entity.RecordStatus;
import com.aditya.kafka.events.repositories.RecordStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class RecordStatusProvider {

    @Autowired
    RecordStatusRepository repository;

    private final Map<String, RecordStatus> recordStatusMap = new HashMap<>();

    @PostConstruct
    private void init(){
        List<RecordStatus> recordStatuses = new ArrayList<>();
        recordStatuses.add(RecordStatus.builder().status("LOADED").build());
        recordStatuses.add(RecordStatus.builder().status("PROCESSING").build());
        recordStatuses.add(RecordStatus.builder().status("COMPLETED").build());
        recordStatuses.add(RecordStatus.builder().status("ERROR").build());

        repository.saveAll(recordStatuses);

        recordStatuses.forEach(f-> recordStatusMap.put(f.getStatus(), f));

    }

    public RecordStatus getRecordStatus(String status){
        return Optional.of(recordStatusMap.get(status)).orElse(null);
    }
}
