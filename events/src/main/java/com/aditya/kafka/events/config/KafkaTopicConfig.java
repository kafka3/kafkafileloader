package com.aditya.kafka.events.config;

import com.aditya.kafka.events.constants.TopicNames;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaTopicConfig {

    @Value("${kafka.bootstrap.address}")
    private String bootstrapAddress;

    @Bean
    public KafkaAdmin kafkaAdmin(){
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic fileTopic(){
        return new NewTopic(TopicNames.FILE_READER, 2, (short)2);
    }

    @Bean
    public NewTopic recordProcessingTopic(){
        return new NewTopic(TopicNames.RECORD_PROCESSING_START, 2, (short)2);
    }
}
