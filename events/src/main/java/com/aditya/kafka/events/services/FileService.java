package com.aditya.kafka.events.services;

import com.aditya.kafka.events.models.entity.Employee;
import com.aditya.kafka.events.models.entity.FileDetails;
import com.aditya.kafka.events.models.entity.QFileDetails;
import com.aditya.kafka.events.repositories.FileRepository;
import com.querydsl.core.BooleanBuilder;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService extends BaseService {

    @Autowired
    private FileRepository repo;

    public void saveFile(FileDetails model){

        if(model != null){
            prepModel(model);
            repo.save(model);
        }

    }

    public List<FileDetails> getFiles(){
        return repo.findAll();
    }

    public FileDetails getFileById(Long id){
        return repo.findById(id).orElse(null);
    }

    public List<FileDetails> getFilesByStatus(Long statusId){
        QFileDetails q = QFileDetails.fileDetails;

        BooleanBuilder where = new BooleanBuilder();
        where.and(q.status.id.eq(statusId));

        return IteratorUtils.toList(repo.findAll(where).iterator());
    }
}
