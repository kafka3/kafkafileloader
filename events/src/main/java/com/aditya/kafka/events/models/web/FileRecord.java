package com.aditya.kafka.events.models.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FileRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String designation;
    private String currencyCode;
    private Double salary;
    private String yearlyOrMonthly;
    private String errorData;
}
