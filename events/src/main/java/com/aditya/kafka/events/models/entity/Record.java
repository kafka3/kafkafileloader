package com.aditya.kafka.events.models.entity;

import org.springframework.context.annotation.Configuration;

import javax.persistence.*;

@Entity
@Table(name = "record")
public class Record extends BaseModel {

    @ManyToOne
    @JoinColumn(name = "file_id")
    private FileDetails fileDetails;

    @ManyToOne
    @JoinColumn(name = "record_status_id")
    private RecordStatus recordStatus;

    @Column(name = "name")
    private String name;

    @Column(name = "designation")
    private String designation;

    @Column(name = "currency_code")
    private String currencyCode;

    @Column(name = "salary")
    private Double salary;

    @Column(name = "monthly_yearly")
    private String yearlyOrMonthly;

    @Column(name = "take_home_salary")
    private Double takeHomeSalary;

    @Column(name = "error_desc")
    private String errorDetails;

    public FileDetails getFileDetails() {
        return fileDetails;
    }

    public void setFileDetails(FileDetails fileDetails) {
        this.fileDetails = fileDetails;
    }

    public RecordStatus getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(RecordStatus recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Double getTakeHomeSalary() {
        return takeHomeSalary;
    }

    public void setTakeHomeSalary(Double takeHomeSalary) {
        this.takeHomeSalary = takeHomeSalary;
    }

    public String getYearlyOrMonthly() {
        return yearlyOrMonthly;
    }

    public void setYearlyOrMonthly(String yearlyOrMonthly) {
        this.yearlyOrMonthly = yearlyOrMonthly;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }
}
