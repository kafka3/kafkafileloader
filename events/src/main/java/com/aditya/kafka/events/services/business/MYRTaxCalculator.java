package com.aditya.kafka.events.services.business;

import com.aditya.kafka.events.constants.CommonConstants;
import com.aditya.kafka.events.services.business.interfaces.ITaxCalculator;
import org.springframework.stereotype.Service;

@Service
public class MYRTaxCalculator implements ITaxCalculator {
    @Override
    public Double processSalary(Double salary, String monthlyOrYearly) {

        Double tax = 0d;

        if(monthlyOrYearly.equalsIgnoreCase(CommonConstants.MONTHLY)){
            salary = salary * 12;
        }

        if(salary <= 5000){
            tax =  0d;
        }
        else if(salary <= 20000){
            tax =  (salary-5000)*(0.01);
        }
        else if(salary <= 35000){
            tax =  150+((salary-20000)*0.03);
        }
        else if(salary <= 50000){
            tax =  600+((salary-35000)*0.08);
        }
        else if(salary <= 70000){
            tax =  1800+((salary-50000)*0.14);
        }
        else if(salary <= 100000){
            tax =  4600+((salary-70000)*0.21);
        }
        else if(salary <= 250000){
            tax =  10900+((salary-100000)*0.24);
        }
        else if(salary <= 400000){
            tax =  46900+((salary-250000)*0.245);
        }
        else if(salary <= 600000){
            tax =  83650+((salary-400000)*0.25);
        }
        else if(salary <= 1000000){
            tax =  133650+((salary-600000)*0.26);
        }
        else if(salary <= 2000000){
            tax =  237650+((salary-1000000)*0.28);
        }
        else if(salary > 2000000){
            tax =  517650+((salary-2000000)*0.3);
        }

        if(monthlyOrYearly.equalsIgnoreCase(CommonConstants.MONTHLY)){
            tax = tax/12;
        }

        return tax;
    }

    @Override
    public String getCurrencyCode() {
        return CommonConstants.MYR;
    }
}
