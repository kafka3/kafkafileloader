package com.aditya.kafka.events.services;

import com.aditya.kafka.events.models.entity.Employee;
import com.aditya.kafka.events.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService extends BaseService {

    @Autowired
    private EmployeeRepository repo;

    public void saveEmployee(Employee model){

        if(model != null){
            prepModel(model);
            repo.save(model);
        }
    }

    public List<Employee> getEmployees(){
        return repo.findAll();
    }
}
