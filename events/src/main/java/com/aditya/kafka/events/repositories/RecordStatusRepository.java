package com.aditya.kafka.events.repositories;

import com.aditya.kafka.events.models.entity.RecordStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordStatusRepository extends JpaRepository<RecordStatus, Long> {
}
