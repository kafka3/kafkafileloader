# Kafka File Loader and Processor
This project is meant to load file(of a specific format) and process it using Kafka. 

To implement this, this project takes a file with Person details and compute take-home salary after deducting tax.

## Initial setup

- Download and start zookeeper and kafka servers
- [Reference](https://kafka.apache.org/quickstart) 

## Run project
```
1. mvn clean install(make sure to start zookeeper and kafka servers)
2. java -jar web/target/web-0.0.1.jar
3. Run localhost:9292/kfl/
```

## About project

- **Events**: This module contains business logic. It will get file contents on Kafka topic, read them and process them.
- **Web**: This is front face of the application. Over here you can upload file, check files and records status.

Basic functionality of this project is to ready member's current salary, compute tax based on currency code and display *Total and Take Home Salary*.

Events uses **Kafka** to process files and records, however web module uses **Thymeleaf** to get and display information.

### File format

File format is most important factor over here otherwise this application is of no use.
```
1. Only csv files can be processed.
2. File shouldn't have any header or footer.
3. File should have exactly 5 data set in a row. These are Name, Designation, Currency code, Salary and Y/N stating whether salary is yearly or monthly.
4. Data should be comma separated
```